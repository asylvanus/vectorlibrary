#!/bin/bash

# Clear out folders
make clean
echo "Folders cleared"

# Compile code
make
echo "Executable generated"

# Build Library
make lib
echo "Library built"

ls

# Test code
./Test

echo "Finished"